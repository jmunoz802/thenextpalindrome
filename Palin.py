import math
import time

def increaseIndex(number, index):
	length = len(number)
	midway = round(length/2)

	if number[index] == '9':
		while number[index] == '9':
			number[index] = '0'
			if index < midway:
				number[length-1-index] = '0'
			if index == 0:
				number = ['0'] + number
				length = length + 1
				break
			index = index - 1
	
	newNum = int(number[index]) + 1
	midway = round(length/2)
	if index < midway:
		number[length-1-index] = str(newNum)

	number[index] = str(newNum)
	
	return number

def nextPalindrome(number):
	length = len(number)

	if length == 0:
		return ''
	
	#Filter out leading 0s
	indx = 0
	for i in range(length):
		if number[i] == '0':
			indx = indx + 1
		else:
			break
	
	if indx == length:
		number = number[0]
	else:
		number = number[indx:]
	length = len(number)
	str_list = list(number)
	
	midway = math.floor((length)/2)
	if length%2 == 0:
		midway -= 1
	
	changed = False
	for indx in range(length-1 , midway, -1):
		back = int(str_list[indx])
		front = int(str_list[length-1-indx])

		if front is not back:
			changed = True
			str_list[indx] = str(front)
			
			if front < back:
				str_list = increaseIndex(str_list, indx-1)

	#If given number is already a palindrome	
	if not changed:
		str_list = increaseIndex(str_list, midway)

	return ''.join(str_list)

#slow but sure method (barring already palindromic numbers). essentially O(n^2)
def sanityCheck(number):
	if not isPalindrome(number):
		while(not isPalindrome(number)):
			num = int(number)+1
			number = str(num)

	return number

#palindrome string tester
def isPalindrome(number):
	length = len(number)

	if length==1 or length == 0:
		return True
	else:
		for i in range(int(length/2)):
			if number[i] != number[length - 1 - i]:
				return False
	return True

#main  I/O
cases = int(input(""))
inputNumber = []
outputNumber = []
#checkNumber = []

for i in range(cases):
	inputNumber = inputNumber + [input("")]
	
#start = time.clock()
for number in inputNumber:
	outputNumber = outputNumber + [nextPalindrome(number)]
	#checkNumber = checkNumber + [sanityCheck(number)]

print("")

for output in outputNumber:
	print(output)
	
#end = time.clock()

#print("start: " + str(start))
#print("stop: " + str(end))
	
#for output in outputNumber:
#	print(isPalindrome(output))
